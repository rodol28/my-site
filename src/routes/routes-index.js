const router = require('express').Router();
const control = require('../controllers/ctrl-index');

router.get('/', control.home);

router.get('/contact', control.contact);

module.exports = router;