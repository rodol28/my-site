const morgan = require('morgan');
const express = require('express');
const router = require('./routes/routes-index.js');
const path = require('path');

const app = express();

//settings 
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, './views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

// //middleware
app.use(morgan('dev'));

// //routes
app.use(router);

//static files
app.use(express.static(path.join(__dirname, 'public')));

//start server
app.listen(app.get('port'), () => {
    console.log(`Server listening on port ${app.get('port')}`)});