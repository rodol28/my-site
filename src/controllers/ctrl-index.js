module.exports = {
    home: (req, res) => {
        res.render('view-index.html', {title: 'My web site'});
    },

    contact: (req, res) => {
        res.render('contact.html', {title: 'Contact'});
    }
};